#!/bin/bash

source /etc/profile.d/rbenv.sh

rbenv shell `head -1 .rbenv-version`

eval `ssh-agent -s`
ssh-add /var/lib/jenkins/.ssh/github

bundle exec cap integration deploy:migrations
