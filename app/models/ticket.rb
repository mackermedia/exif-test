class Ticket < ActiveRecord::Base
  attachment :image

  validates :title, :image, :presence => true

  before_save :store_gps_coordinates

  private

  def store_gps_coordinates
    if image_exifr.gps.present?
      self.lat = image_exifr.gps.latitude
      self.lng = image_exifr.gps.longitude
    end
  end

  def image_exifr
    @image_exifr ||= EXIFR::JPEG.new(image.to_io)
  end
end
