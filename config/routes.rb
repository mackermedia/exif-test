Rails.application.routes.draw do

  # manually add the refile route to work with our catch-all route
  mount Refile.app, at: "/attachments", as: :refile_app

  root :to => 'tickets#index'

  resources :tickets, :only => [:new, :create, :show, :index]

  # need this because Rails `rescue_from` doesn't catch ActionController::RoutingError
  unless Rails.env.development?
    match '*path',  :to => 'application#render_404', :via => :all
  end
end
