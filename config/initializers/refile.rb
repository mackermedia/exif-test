Refile.automount = false

if Rails.env.production?
  require "refile/backend/s3"

  aws = {
    access_key_id: ENV["AWS_KEY"],
    secret_access_key: ENV["AWS_SECRET"],
    bucket: "exif-test",
  }
  Refile.cache = Refile::Backend::S3.new(prefix: "cache", **aws)
  Refile.store = Refile::Backend::S3.new(prefix: "store", **aws)

  Refile.host = "http://d7urpzfib6eyc.cloudfront.net"
end
