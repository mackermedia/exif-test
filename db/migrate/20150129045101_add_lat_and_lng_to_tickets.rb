class AddLatAndLngToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :lat, :decimal, { :precision => 10, :scale => 6 }
    add_column :tickets, :lng, :decimal, { :precision => 10, :scale => 6 }
  end
end
