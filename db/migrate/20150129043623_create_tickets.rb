class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :title,    :null => false
      t.string :image_id, :null => false

      t.timestamps
    end
  end
end
